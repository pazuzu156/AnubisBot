# AnubisBot.NET
AnubisBot is dead. I haven't made any updates to the bot in over a year. The library is very out of date and likely won't work in the near future due to newer Discord API versions. You're welcome to fork the project and go on with it. I don't forsee myself returning to the bot as I have been working on a different one for the past year. [Persephone](https://github.com/pazuzu156/persephone)

## About
This is my attempt to port AnubisBot over from PHP to C#. AnubisBot.NET uses .NET Core 2.0, so you need this installed to compile the bot.

## Requirements
### SDKs
[.NET Core 2.0](https://www.microsoft.com/net/core) <- for debugging in VSCode   and building on Linux
[.NET Framework 4.6.1](https://www.microsoft.com/en-us/download/details.aspx?id=49978) <- for releasing on Windows

### Libraries
[DiscordBotsList](https://github.com/DiscordBotList/DBL-dotnet-Library) <- Allows you to send server count to DiscordBots website
[DSharpPlus](https://github.com/NaamloosDT/DSharpPlus) <- Discord API wrapper for C#
[GoogleApis](https://www.nuget.org/packages/Google.Apis.youtube.v3/) <- Google API for YouTube
[Humanizer](http://humanizr.net/) <- For making DateTime look better
[Inflatable.Lastfm](https://github.com/inflatablefriends/lastfm) <- Lastfm API client
[JSON.NET](https://www.newtonsoft.com/json) <- Newtonsoft JSON library
[ServiceStack.OrmLite](https://servicestack.net/ormlite) <- The MySQL ORM used for interfacing with databases

### Extras
There's a few extra libraries you must have to make the bot run properly. For Windows, download [Python](https://www.python.org/), [ffmpeg](https://ffmpeg.org/), and the [Opus and Sodium](https://dsharpplus.emzi0767.com/natives/index.html) libraries. After installing Python, run `pip install youtube-dl` to install youtube-dl. Make sure you add the paths to ffmpeg and Opus/Sodium to your PATH environment variable so the bot can find them

On Linux, install the following:
`libsodium opus ffmpeg`

Depending on your distro, you can install `youtube-dl` through their package manager. Be sure to install the .NET Core sdk, which can be obtained from the link above.

Some distros require both libsodium/opus packages AND their respective dev packages. If so, install these too.

Arch
Arch is easy, clone this repo, and checkout the `arch` branch, then just run `makepkg -is` This will install all required dependencies, and compile/install AnubisBot.

## Compiling/Running
### Libraries
The project was structured with .NET Core 2.0 in mind, and supports .NET Framework 4.6.1, which is used to build the releases. With that, you can use the .NET Core cli tools to build and run the project.

To get the latest defined libraries used by AnubisBot, using the .NET Core tools, run `$ dotnet restore`

### Building
If you have .NET Core, you can do so from the terminal. `$ dotnet build` or `$ dotnet run`

Linux and Windows have build scripts used to build/package the apps and run test builds. They are both used in the same way

`./build` <- Runs the build/packaging
`./build -r` <- Starts a rebuild and test run
`./build -c` <- Cleans up all binaries and compiled shit
`./build -h` <- Need help? Run this

Windows works exactly the same way, just remove the `./` at the start of the command

I've also included a Makefile for Linux users, simply run `$ make; sudo make install` to build and install AnubisBot

After building, Linux users can install using the provided Makefile, and run the bot using the `anubisbot` command, or using the systemd service `$ sudo systemctl enable --now anubisbot.service`

### Installer
Installer is built with Inno Setup 5. So you need to download and install that. The installer also uses VCL Styles to style it, you need this plugin for Inno Setup. Simply run the build script and it will build the app AND the installer

For Linux, it'll package the published bit into a `.tgz` archive and place it in the project's root directory

This project was set up and programmed using [Visual Studio Code](https://code.visualstudio.com/), I recommend using that

## How to Use This Bot
There are 2 ways you can have this bot, either by compiling and hosting it yourself, or by inviting the bot I host. The only customization you'll have are guild specific ones if you use the bot I host. For full customization, you should host it yourself.

If you want to invite the bot, use this link: https://discordapp.com/api/oauth2/authorize?client_id=327322469197283328&permissions=268561462&scope=bot

## Guide
Please refer to `guide.md` for a list of commands currently built into the bot
