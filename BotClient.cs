using System;
using System.Threading.Tasks;
using AnubisBot.Data;
using AnubisBot.Utils;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using DSharpPlus.EventArgs;
using ServiceStack.OrmLite;

namespace AnubisBot
{
    public class BotClient
    {
        private DiscordShardedClient Client;

        private JsonData _json;

        private PresenceUpdater _presenceUpdater;

        private int _presenceIndex = 1;

        private bool IsClientReady = false;

        public BotClient(DiscordShardedClient client)
        {
            this.Client = client;
            this._json = Settings.getInstance().config;
            this._presenceUpdater = new PresenceUpdater(this._json, this.Client);

            client.Ready += Client_Ready;

            // closing these two because there's some problems in the code I need to fix to stop bot from dying all the time
            client.Heartbeated += Client_Heartbeated;
            // client.GuildCreated += Client_GuildCreated;
            client.GuildMemberAdded += Client_GuildMemberAdded;
            client.GuildMemberRemoved += Client_GuildMemberRemoved;
        }

        /// <summary>
        /// Client ready event handler
        /// </summary>
        /// <param name="e">Ready event arguments</param>
        /// <returns>System.Threading.Tasks.Task.CompletedTask</returns>
        private async Task Client_Ready(ReadyEventArgs e)
        {
            var client = e.Client as DiscordClient;

            Console.CancelKeyPress += async delegate {
                await client.DisconnectAsync();
            };

            string[] msgs = {
                "Bot is ready",
                $"Bot name: {client.CurrentUser.Username}",
                $"Bot ID: {client.CurrentUser.Id}"
            };

            foreach (var msg in msgs) {
                client.DebugLogger.LogMessage(LogLevel.Info, Program.LOG_TAG, msg, DateTime.Now);
            }

            try {
                await _presenceUpdater.Update(0);
                client.DebugLogger.LogMessage(LogLevel.Debug, Program.LOG_TAG, "Presence updated", DateTime.Now);
            } catch {
                client.DebugLogger.LogMessage(LogLevel.Error, Program.LOG_TAG, "Could not update presence", DateTime.Now);
            }

            try {
                if (_json.DiscordBots.Announce) {
                    await DiscordBotsAnnounce.Send(client);
                }
            } catch (Exception ex) {
                client.DebugLogger.LogMessage(LogLevel.Error, Program.LOG_TAG, "An error occurred when attempting to send announcement to DiscordBots\n" + ex.Message, DateTime.Now);
            }

            IsClientReady = true;
        }

        private async Task Client_Heartbeated(HeartbeatEventArgs e)
        {
            if (IsClientReady) {
                if (_presenceIndex > _json.PresenceStrings.Length - 1) {
                    _presenceIndex = 0;
                }

                await _presenceUpdater.Update(_presenceIndex);
                _presenceIndex++;
            }

            UpdateGuildList(e.Client as DiscordClient);
        }

        private Task Client_GuildCreated(GuildCreateEventArgs e)
        {
            UpdateGuildList(e.Client as DiscordClient);

            return Task.CompletedTask;
        }

        private async Task Client_GuildMemberAdded(GuildMemberAddEventArgs e)
        {
            if (areMessagesEnabled(e.Guild)) {
                var msg = Utils.MessageFilter.init(e.Guild, e.Member).Replace(joinMessage(e.Guild), @"\{([A-Z_]+)\}");
                await GetBotSpam(e.Guild).SendMessageAsync(msg);
            }
        }

        private async Task Client_GuildMemberRemoved(GuildMemberRemoveEventArgs e)
        {
            if (areMessagesEnabled(e.Guild)) {
                var msg = Utils.MessageFilter.init(e.Guild, e.Member).Replace(leaveMessage(e.Guild), @"\{([A-Z_]+)\}");
                // Client.DebugLogger.LogMessage(LogLevel.Debug, "MEMBER_REMOVED", msg, DateTime.Now);
                await GetBotSpam(e.Guild).SendMessageAsync(msg);
            }
        }

        /// <summary>
        /// Commands executed event handler
        /// </summary>
        /// <param name="e">Command execution event arguments</param>
        /// <returns>System.Threading.Tasks.Task.CompletedTask</returns>
        public Task Commands_CommandExecuted(CommandExecutionEventArgs e)
        {
            e.Context.Client.DebugLogger.LogMessage(LogLevel.Debug, Program.LOG_TAG, $"{e.Context.User.Username} successfully executed '{e.Command.QualifiedName}'", DateTime.Now);

            return Task.CompletedTask;
        }

        private void UpdateGuildList(DiscordClient client)
        {
            foreach (DiscordGuild guild in client.Guilds.Values) {
                var db = Sql.Database.Connect();
                var guildList = db.Select<Sql.Guilds>(q => q.guild_id == guild.Id);

                if (guildList.Count == 0) {
                    db.Insert(new Sql.Guilds {
                        name = guild.Name,
                        guild_id = guild.Id,
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    });
                }
            }
        }

        public static DiscordChannel GetBotSpam(DiscordGuild guild)
        {
            var db = Sql.Database.Connect();
            var bs = db.Single<Sql.BotSpam>(q => q.guild_id == guild.Id);
            db.Close();

            if (bs != null) {
                return guild.GetChannel(bs.channel_id);
            }

            return null;
        }

        private bool areMessagesEnabled(DiscordGuild guild)
        {
            var db = Sql.Database.Connect();
            var bs = db.Single<Sql.BotSpam>(q => q.guild_id == guild.Id);
            db.Close();

            if (bs != null) {
                return bs.is_enabled;
            }

            return false;
        }

        private string joinMessage(DiscordGuild guild)
        {
            var db = Sql.Database.Connect();
            var bs = db.Single<Sql.BotSpam>(q => q.guild_id == guild.Id);
            db.Close();

            if (bs != null) {
                if (bs.join_message != "") {
                    return Uri.UnescapeDataString(bs.join_message);
                }
            }

            return "{USER} has joined the server!";
        }

        private string leaveMessage(DiscordGuild guild)
        {
            var db = Sql.Database.Connect();
            var bs = db.Single<Sql.BotSpam>(q => q.guild_id == guild.Id);
            db.Close();

            if (bs != null) {
                if (bs.leave_message != "") {
                    return Uri.UnescapeDataString(bs.leave_message);
                }
            }

            return "{USER} has left the server!";
        }
    }
}
