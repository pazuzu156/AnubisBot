# AnubisBot.NET Guide
This is a simple command guide to help you understand how to use the bot and it's current built in commands.

*All commands here will assume the default prefix is set to `-`*

## User Management
User and guild management are a bit part of this bot, since it is a moderation bot. The following commands are how to manage users in your server.

> `-ban <@Member> [Reason]` Bans a user (Requires Ban Members Permission)
>> \<@Member> is the member to ban (Required)  
>> [Reason] is the reason why the user is being banned (sent to user and audit log)

Ex: `-ban @Anubis#8384 This is an example ban`

> `-kick <@Member> [Reason]` Kicks a user (Requires Kick Members Permission)
>> \<@Member> is the member to kick (Required)  
>> [Reason] is the reason why the user is being kicked (sent to user and audit log)

Ex: `-kick @Anubis#8384 This is an example kick`

## Message Management
Being able to manage messages, to delete or prune multiple messages is a really useful feature. AnubisBot can do these with ease

> `-prune [Count]` Deletes messages (Requires Manage Messages Permission)
>> [Count] The number of messages to remove (Default is 50 if left empty in the request)

Ex: `-prune 10`

> `-prune user <@Member> [Count]` Deletes a user's messages (Requires Manage Messages Permission)
>> \<@Member> the member to delete the messages for (Required)  
>> [Count] The number of messages to remove (Default is 50 if left empty in the request)

Ex: `-prune user @Anubis#8384 10`

> `-prune bot [Count]` Deletes the bot's messages (Requires Manage Messages Permission)
>> [Count] The number of messages to remove (Default is 50 if left empty in the request)

Ex: `-prune bot 10`

## Role Management
The bot knows all of the roles in a server, so you need to manually hide servers from users. Thankfully, this is easy to do with AnubisBot

> `-roles` Get a list of joinable roles

> `-roles join <Role>` Join a role, or list of roles
>> \<Role> The role, or list of roles to join

*You can join individual roles, or a list of roles, seperated by comas*
Ex: `-roles join some role`
Ex: `-roles join some role,another role`

> `-roles leave <Role>` Leave a role, or list of roles
>> \<Role> The role, or list of roles to leave

*You can leave individual roles, or a list of roles, seperated by comas*
Ex: `-roles leave some role`
Ex: `-roles leave some role,another role`

> `-roles restrict <Role>` Restrict a role, or list of roles (Requires Manage Roles Permission)
>> \<Role> The role, or list of roles to leave

*You can restrict individual roles, or a list of roles, seperated by comas*
Ex: `-roles restrict some role`
Ex: `-roles restrict some role,another role`

> `-roles unrestrict <Role>` Unrestrict a role, or list of roles (Requires Manage Roles Permission)
>> \<Role> The role, or list of roles to leave

*You can unrestrict individual roles, or a list of roles, seperated by comas*
Ex: `-roles unrestrict some role`
Ex: `-roles unrestrict some role,another role`

## Bot Management
The bot needs to interface with the guild too. Want custom greetings, and a dedicated logging channel for the bot to put it's shitty logs? Use one of thse commands

> `-botmod setbotspam <#Channel>` Set the bot's spam channel (Requires the Manage Guild Permission)
>> \<#Channel> The channel the bot should send its logs to (Required)

Ex: `-botmod setbotspam #mod-logs`

> `-botmod joinmsg <Message>` Set a custom welcome message (Requires the Manage Guild Permission)
>> \<Message> The custom message to set (Required)

*Custom messages also have variables for them, listed below*
Ex: `-botmod joinmsg Welcome to {GUILD} {USER}! Please enjoy your stay!`

`{USER}` - Displays the user to send the greeting to
`{GUILD}` - The name of the guild

> `-botmod leavemsg <Message>` Set a custom dismissal message (Requires the Manage Guild Permission)
>> \<Message> The custom message to set (Required)

*Custom messages also have variables for them, listed below*
Ex: `-botmod leavemsg {USER} has left {GUILD}. :cry:`

`{USER}` - Displays the user to send the greeting to  
`{GUILD}` - The name of the guild

> `-botmod deljoinmsg` Delete the custom welcome message (Requires the Manage Guild Permission)

> `-botmod delleavemsg` Delete the custom dismissal message (Requires the Manage Guild Permission)

> `-botmod enablemessage <True|False>` Enable/Disable custom greetings (Requires the Manage Guild Permission)
>> <True|False> true for enable, false for disable (Required)

## Information
There's a few info commands to. Need info on the bot, the guild, or a given user? Use one of these

> `-info [@Member]` Get info on yourself or another user
>> [@Member] The member to get the info of

Ex: `-info` `-info @Anubis#8384`

> `-info getid` Get your Discord ID in a DM

## Steam
As of v0.3, a new Steam command was added, removing the steam command from `-info` and adding it to the Steam command group

> `-steam discount <appId> <saleEnd> [channel]`
>> \<appId> The App ID of the game/app currently on sale (Required)  
>> \<saleEnd> When the sale ends (Format: HH:MM:SS) (Required)  
>> [channel] The channel to send the discount embed message to (Optional)

Ex: `-steam discount 70 24-05-00 #discounts`

> `-steam expire <messageId> [channel]`
>> \<messageId> Message ID of the embed to expire (Required)  
>> \[channel] The channel where the message to modify can be found (Optional: Will fail if message is in another channel)

Ex: `-steam expire 536008297153691649 #discounts`

> `-steam getid <Username>` Get the Steam64 ID of a given Steam user
>> \<Username> The user's Steam username (Required)

Ex: `-steam getid Pazuzu156`
