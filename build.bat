@echo off
title AnubisBot Build Script

set CWD=%~dp0
set ISS_HOME=C:\Program Files (x86)\Inno Setup 5
set ISS="%ISS_HOME%\ISCC.exe"

rem Version
for /f "delims=" %%i in (version.txt) do set VERSION=%%i

if "%1" NEQ "" (
    if "%1" EQU "-c" goto clean
    if "%1" EQU "-d" goto restore
    if "%1" EQU "-h" goto help
    if "%1" EQU "-i" goto installer
    if "%1" EQU "-r" goto run
)

:build
dotnet build -c Release -r win10-x64 -f net461 -v d
echo build complete
if "%1" NEQ "" (
    if "%1" EQU "-n" (
        goto end
    ) else (
        goto askinstaller
    )
) else (
    goto askinstaller
)

:askinstaller
set /p install="Build installer? (Y/N) "
if "%install%"=="Y" (
    goto installer
) else if "%install%"=="y" (
    goto installer
) else (
    goto end
)

:restore
dotnet restore
goto end

:installer
cd setup
%ISS% setup.iss /dVersion=%VERSION%
cd ..
goto end

:run
dotnet run -f netcoreapp2.0
goto end

:clean
if exist bin rmdir /S /Q bin
if exist obj rmdir /S /Q obj
cd setup
if exist Installer rmdir /S /Q Installer
cd %CWD%
echo Cleaning finished
goto end

:help
echo AnubisBot Windows Build Script 1.0
echo Usage: build.bat [option]
echo Supplying no option will cause the build tool to build the app
echo.
echo Options
echo    -c              - Runs the binary cleanup
echo    -d              - Runs the dotnet restore command
echo    -h              - Shows this help text
echo    -i              - Generates the installer (fails if not built first)
echo    -n              - Build without being asked to build installer
echo    -r              - Runs a debug build of the app
goto end

:end
