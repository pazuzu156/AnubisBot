using AnubisBot.Data;
using DSharpPlus;
using System;

namespace AnubisBot.Utils
{
    public class Logger
    {
        /// <summary>
        /// Discord client object.
        /// </summary>
        private DiscordClient _client;

        /// <summary>
        /// Settings data.
        /// </summary>
        private JsonData _settings;

        /// <summary>
        /// Application logger.
        /// </summary>
        /// <param name="client">The discord client to log under</param>
        public Logger(DiscordClient client)
        {
            this._client = client;
            this._settings = Settings.getInstance().config;
        }

        /// <summary>
        /// Sends a log message to the client
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="tag">Log message tag</param>
        /// <param name="logLevel">Log level</param>
        public void LogMessage(string message, string tag, LogLevel logLevel = LogLevel.Info)
        {
            this._client.DebugLogger.LogMessage(logLevel, tag, message, DateTime.Now);
        }

        /// <summary>
        /// Sends a critical log message to the client
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="tag">Log message tag</param>
        public void Critical(string message, string tag = "CRITICAL")
        {
            this.LogMessage(message, tag, LogLevel.Critical);
        }

        /// <summary>
        /// Sends a debug log message to the client
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="tag">Log message tag</param>
        public void Debug(string message, string tag = "DEBUG")
        {
            this.LogMessage(message, tag, LogLevel.Debug);
        }

        /// <summary>
        /// Sends an error log message to the client
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="tag">Log message tag</param>
        public void Error(string message, string tag = "ERROR")
        {
            this.LogMessage(message, tag, LogLevel.Error);
        }

        /// <summary>
        /// Sends an info log message to the client
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="tag">Log message tag</param>
        public void Info(string message, string tag = "INFO")
        {
            this.LogMessage(message, tag, LogLevel.Info);
        }

        /// <summary>
        /// Sends a warning log message to the client
        /// </summary>
        /// <param name="message">The message to send</param>
        /// <param name="tag">Log message tag</param>
        public void Warning(string message, string tag = "WARNING")
        {
            this.LogMessage(message, tag, LogLevel.Warning);
        }
    }
}
