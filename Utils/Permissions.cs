using DSharpPlus;
using DSharpPlus.Entities;

namespace AnubisBot.Utils
{
    public class Permissions
    {
        /// <summary>
        /// Checks to see of a user has a specified permission
        /// </summary>
        /// <param name="member">The member to check</param>
        /// <param name="permission">The permission to check for the given member</param>
        /// <returns>System.Boolean</returns>
        public static bool Can(DiscordMember member, DSharpPlus.Permissions permission)
        {
            bool can = false;

            foreach (var role in member.Roles) {
                if (role.CheckPermission(permission) == PermissionLevel.Allowed) {
                    can = true;
                }
            }

            return can;
        }
    }
}
