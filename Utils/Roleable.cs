using AnubisBot.Commands;
using DSharpPlus.Entities;
using ServiceStack.OrmLite;
using System.Collections.Generic;

namespace AnubisBot.Utils
{
    /// <summary>
    /// Parent class used for commands that need any role specific methods. (Extends BaseCommand)
    /// </summary>
    public class Roleable : BaseCommand
    {
        /// <summary>
        /// Gets a list of roles that have been restricted for a give Guild.
        /// </summary>
        /// <param name="guild">The guild to use for role search</param>
        /// <returns>List of restricted roles</returns>
        protected List<DiscordRole> getRestrictedRoles(DiscordGuild guild)
        {
            var restrictedRoles = new List<DiscordRole>();

            foreach (var role in guild.Roles) {
                if (role.Name == "@everyone") {
                    restrictedRoles.Add(role);
                }

                var db = Sql.Database.Connect();
                var roles = db.Select<Sql.RestrictedRoles>(
                    q => q.guild_id == guild.Id
                );
                db.Close();

                foreach (var rRole in roles) {
                    restrictedRoles.Add(guild.GetRole(rRole.role_id));
                }
            }

            return restrictedRoles;
        }

        /// <summary>
        /// Gets a list of roles that are joinable. (non-restricted roles)
        /// </summary>
        /// <param name="guild">The guild to use for role search</param>
        /// <returns>List of joinable roles</returns>
        protected List<DiscordRole> getJoinableRoles(DiscordGuild guild)
        {
            var joinableRoles = new List<DiscordRole>();

            foreach (var role in guild.Roles) {
                var restrictedRoles = getRestrictedRoles(guild);

                if (!restrictedRoles.Contains(role)) {
                    joinableRoles.Add(role);
                }
            }

            return joinableRoles;
        }

        /// <summary>
        /// Checks if a given role is restricted.
        /// </summary>
        /// <param name="role">The role to check</param>
        /// <returns>True if restricted, false otherwise</returns>
        protected bool isRoleRestricted(DiscordRole role)
        {
            var db = Sql.Database.Connect();
            var roles = db.Select<Sql.RestrictedRoles>(
                q => q.role_id == role.Id
            );
            db.Close();

            return (roles.Count > 0) ? true : false;
        }

        /// <summary>
        /// Returns DiscordRole instance of given role name from a string.
        /// </summary>
        /// <param name="guild">The guild used to find said role</param>
        /// <param name="name">The string representation of said role</param>
        /// <returns>DiscordRole instance of searched role</returns>
        protected DiscordRole getRoleFromName(DiscordGuild guild, string name)
        {
            foreach (var role in guild.Roles) {
                if (role.Name.ToLower() == name) {
                    return role;
                }
            }

            return null;
        }

        /// <summary>
        /// Checks if a user is currently assigned a role.
        /// </summary>
        /// <param name="member">The member to check against</param>
        /// <param name="role">The role to check for</param>
        /// <returns>True if user is assigned role, false otherwise</returns>
        protected bool hasRole(DiscordMember member, DiscordRole role)
        {
            foreach (var userRole in member.Roles) {
                if (role == userRole) {
                    return true;
                }
            }

            return false;
        }
    }
}
