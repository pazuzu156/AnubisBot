using DSharpPlus.Entities;
using System.Text.RegularExpressions;

namespace AnubisBot.Utils
{
    public class MessageFilter
    {
        private DiscordGuild _guild;

        private DiscordMember _member;

        private MessageFilter(DiscordGuild guild, DiscordMember member)
        {
            this._guild = guild;
            this._member = member;
        }

        /// <summary>
        /// Initializes the message filter.
        /// </summary>
        /// <param name="guild">The guild to get info from</param>
        /// <param name="member">The member to get info from</param>
        /// <returns></returns>
        public static MessageFilter init(DiscordGuild guild, DiscordMember member)
        {
            return new MessageFilter(guild, member);
        }

        /// <summary>
        /// Handles the string replacement.
        /// </summary>
        /// <param name="message">The message to run the filter on</param>
        /// <param name="regex">The pattern to filter out</param>
        /// <returns></returns>
        public string Replace(string message, string regex)
        {
            return Regex.Replace(message, regex, ReplacementCallback);
        }

        private string ReplacementCallback(Match match)
        {
            string r = string.Empty;

            switch (match.Groups[1].Value) {
                case "USER":
                    r = _member.Mention;
                    break;
                case "GUILD":
                    r = _guild.Name;
                    break;
            }

            return r;
        }
    }
}
