using System;
using System.Threading.Tasks;
using AnubisBot.Data;
using DiscordBotsList.Api;
using DSharpPlus;

namespace AnubisBot.Utils
{
    class DiscordBotsAnnounce
    {
        const string LOG_TAG = "Announce";

        private DiscordBots db;

        private DiscordBotsAnnounce()
        {
            this.db = Settings.getInstance().config.DiscordBots;
        }

        /// <summary>
        /// Sends guild count to DiscordBots
        /// </summary>
        /// <param name="bot"></param>
        /// <returns></returns>
        public static async Task Send(DiscordClient bot)
        {
            bot.DebugLogger.LogMessage(LogLevel.Info, LOG_TAG, "Sending server count data to DiscordBots", DateTime.Now);
            var a = new DiscordBotsAnnounce();
            var dbauth = new AuthDiscordBotListApi(bot.CurrentUser.Id, a.db.ApiKey);
            await dbauth.UpdateStats(bot.Guilds.Count);
        }
    }
}
