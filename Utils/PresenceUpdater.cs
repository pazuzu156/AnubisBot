using System.Text.RegularExpressions;
using System.Threading.Tasks;
using AnubisBot.Data;
using DSharpPlus;
using DSharpPlus.Entities;

namespace AnubisBot.Utils
{
    public class PresenceUpdater
    {
        private JsonData _config;

        private DiscordShardedClient _client;

        public PresenceUpdater(JsonData config, DiscordShardedClient client)
        {
            this._config = config;
            this._client = client;
        }

        public async Task Update(int index)
        {
            var presence = Regex.Replace(_config.PresenceStrings[index], @"\{([A-Z_]+)\}", ReplacementCallack);
            await _client.UpdateStatusAsync(new DiscordGame(presence));
        }

        /// <summary>
        /// Handles regex replacements on presence string.
        /// </summary>
        /// <param name="match">System.Text.RegularExpressions.Match object</param>
        /// <returns>System.String</returns>
        private string ReplacementCallack(Match match)
        {
            string r = "";

            foreach (var clientShard in this._client.ShardClients) {
                var client = clientShard.Value;

                switch (match.Groups[1].Value) {
                    case "S":
                        r = (client.Guilds.Count == 1) ? "" : "s";
                        break;
                    case "GUILD_COUNT":
                        r = $"{client.Guilds.Count}";
                        break;
                    case "SHARD_ID":
                        r = $"{client.ShardId}";
                        break;
                    case "SHARD_COUNT":
                        r = $"{client.ShardCount}";
                        break;
                    case "PREFIX":
                        r = _config.Prefix + ((_config.PrefixSpace) ? " " : "");
                        break;
                }
            }

            return r;
        }
    }
}
