using System.Collections;

namespace AnubisBot.Utils
{
    public class EnumIterator
    {
        /// <summary>
        /// Gets the number of items in an enum interface.
        /// </summary>
        /// <param name="iEnum"></param>
        /// <returns></returns>
        public static int Count(IEnumerable iEnum)
        {
            int count = 0;

            foreach (var i in iEnum) {
                count++;
            }

            return count;
        }
    }
}
