using DSharpPlus;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.IO;
using System.Runtime.InteropServices;
using AnubisBot.Data;

namespace AnubisBot.Utils
{
    public class Settings
    {
        /// <summary>
        /// Settings file.
        /// </summary>
        private static string SettingsFile = "config.json";

        /// <summary>
        /// Settings config data object.
        /// </summary>
        public JsonData config;

        /// <summary>
        /// Returns whether or not the config file was generated.
        /// </summary>
        public bool Generated = false;

        /// <summary>
        /// Gets the settings instance.
        /// </summary>
        /// <returns>AnubisBot.Settings</returns>
        public static Settings getInstance() { return new Settings(); }

        /// <summary>
        /// Application settings.
        /// </summary>
        private Settings()
        {
            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
                SettingsFile = $"/etc/AnubisBot/config.json";
            }

            if (exists()) {
                LoadSettings();
            } else {
                CreateSettings();
            }
        }

        /// <summary>
        /// Loads application settings.
        /// </summary>
        private void LoadSettings()
        {
            using (var reader = new StreamReader(File.OpenRead(SettingsFile))) {
                config = JsonConvert.DeserializeObject<JsonData>(reader.ReadToEnd());
            }
        }

        /// <summary>
        /// Creates application settings.
        /// </summary>
        private void CreateSettings()
        {
            Generated = true;

            using (var writer = new StreamWriter(File.Create(SettingsFile))) {
                var data = new JsonData {
                    Token = string.Empty,
                    Prefix = ".",
                    PrefixSpace = false,
                    UseInternalLogHandler = true,
                    LogLevel = LogLevel.Info,
                    PresenceStrings = new string[] {
                        "God of {GUILD_COUNT} Underworld{S}",
                        "Try {PREFIX}help"
                    },
                    ShardCount = 1,

                    // register default database info
                    Database = new DatabaseInfo() {
                        Host = "127.0.0.1",
                        Port = 3306,
                        Username = "username",
                        Password = "password",
                        DatabaseName = "mydatabasename"
                    },

                    // Last.fm default info
                    LastFM = new LastFM() {
                        ApiKey = "",
                        Secret = ""
                    },

                    // DiscordBots Server Announce via API
                    DiscordBots = new DiscordBots() {
                        ApiKey = "",
                        Announce = false
                    },

                    // YouTube Data API info
                    YouTube = new YouTube() {
                        Token = ""
                    },

                    // register default command list
                    CommandList = new List<string>() {
                        "AnubisBot.Commands.About",
                        "AnubisBot.Commands.BotMod",
                        "AnubisBot.Commands.Prune",
                        "AnubisBot.Commands.Steam",
                        "AnubisBot.Commands.User.Ban",
                        "AnubisBot.Commands.User.Kick",
                        "AnubisBot.Commands.User.Roles",
                        "AnubisBot.Commands.User.UserInfo"
                    }
                };

                writer.Write(JsonConvert.SerializeObject(data, Formatting.Indented));
            }

            LoadSettings();
        }

        /// <summary>
        /// Checks if the settings file exists.
        /// </summary>
        /// <returns>System.Boolean</returns>
        private bool exists()
        {
            return File.Exists(SettingsFile);
        }
    }
}
