using DSharpPlus;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.CommandsNext.Converters;
using DSharpPlus.Entities;
using System;
using System.Text;
using System.Threading.Tasks;

namespace AnubisBot.Commands
{
    [Group("about", CanInvokeWithoutSubcommand = true)]
    [Description("Gets info about the bot")]
    public class About
    {
        private const string _inviteLink = "https://discord.gg/Bg3wQs2";

        private const string _sourceUrl = "https://github.com/pazuzu156/anubisbot";

        private const long _permissions = 268823574;

        private const ulong _clientId = 327322469197283328;

        public async Task ExecuteGroupAsync(CommandContext ctx)
        {
            await DisplayBotInfo(ctx);

            var prefix = Program.Prefix;

            var sb = new StringBuilder();
            var member = getMemberFromBot(ctx);
            sb.Append($"Want to find out more about {member.Username}?\n");
            sb.Append($"Type `{prefix}about all`, `{prefix}about uptime`, `{prefix}about source`, `{prefix}about invite`, or `{prefix}about invitebot`");

            await ctx.RespondAsync(sb.ToString());
        }

        [Command("uptime")]
        [Description("Displays the amount of time the bot has been live")]
        public async Task Uptime(CommandContext ctx)
        {
            var delta = DateTime.Now - Program.StartTime;
            var days = delta.Days.ToString("n0");
            var hours = delta.Hours.ToString("n0");
            var minutes = delta.Minutes.ToString("n0");
            var seconds = delta.Seconds.ToString("n0");

            var builder = new StringBuilder();

            if (!days.Equals("0")) {
                builder.Append($"{days} days ");
            }

            if (!hours.Equals("0")) {
                builder.Append($"{hours} hours ");
            }

            if (!minutes.Equals("0")) {
                builder.Append($"{minutes} minutes ");
            }

            builder.Append($"{seconds} seconds");

            await ctx.RespondAsync($"Uptime: {builder.ToString()}");
        }

        [Command("source"), Description("Gives the Github link to the source code")]
        public async Task Source(CommandContext ctx)
        {
            await ctx.RespondAsync($"{_sourceUrl}");
        }

        [Command("invite"), Description("Gives the official server's invite link")]
        public async Task Invite(CommandContext ctx)
        {
            var msg = "If you're on my server, you can invite others using the invite link. If you're not on my server, you're welcome to join by also using the invite link.";

            await ctx.RespondAsync(msg + '\n' + _inviteLink);
        }

        [Command("invitebot"), Description("Brings up an invite link for the bot")]
        public async Task InviteBot(CommandContext ctx)
        {
            var baseurl = "https://discordapp.com/oauth2/authorize?scope=bot";
            var url = baseurl + "&client_id=" + _clientId + "&permissions=" + _permissions;

            await ctx.RespondAsync($"You can use this url: {url} to invite the bot to your server");
        }

        [Command("all"), Description("Runs all of the about sub commands")]
        public async Task All(CommandContext ctx)
        {
            await Source(ctx);
            await Invite(ctx);
            await InviteBot(ctx);
        }

        private DiscordMember getMemberFromBot(CommandContext ctx)
        {
            var mc = new DiscordMemberConverter();
            DiscordMember member;
            mc.TryConvert(ctx.Client.CurrentUser.Username, ctx, out member);

            return member;
        }

        private async Task DisplayBotInfo(CommandContext ctx)
        {
            var member = getMemberFromBot(ctx);
            var version = Program.Version.ToString(3) + "-beta";
            ctx.Client.DebugLogger.LogMessage(LogLevel.Debug, "AnubisBot", $"Version: {version}", DateTime.Now);

            ctx.Client.DebugLogger.LogMessage(LogLevel.Debug, "AnubisBot", $"Member: {member.ToString()}", DateTime.Now);

            var rolesBuilder = new StringBuilder();

            foreach (var role in member.Roles) {
                rolesBuilder.Append(role.Name + ", ");
            }

            var embed = new DiscordEmbedBuilder {
                Title = $"About {member.DisplayName}",
                Color = DiscordColor.Chartreuse,
                ThumbnailUrl = member.AvatarUrl,
                Description = $"{member.DisplayName} is a bot wirtten in C#. Version {version}",
                Timestamp = DateTime.UtcNow
            };

            embed.AddField("Name", $"{member.DisplayName}#{member.Discriminator}");
            embed.AddField("ID", member.Id.ToString());

            if (member.Nickname != null) {
                embed.AddField("Nickname", member.Nickname);
            }

            embed.AddField("Roles", rolesBuilder.ToString().TrimEnd(new char[] { ',', ' ' }));
            embed.AddField("Server Count", ctx.Client.Guilds.Count.ToString());

            await ctx.RespondAsync("", embed: embed);
        }
    }
}
