using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using Humanizer.DateTimeHumanizeStrategy;
using System;
using System.Globalization;
using System.Text;
using System.Threading.Tasks;

namespace AnubisBot.Commands.User
{
    [Group("info", CanInvokeWithoutSubcommand = true), Description("Gets user info")]
    public class UserInfo
    {
        public async Task ExecuteGroupAsync(CommandContext ctx, [RemainingText, Description("The user to get info on")] DiscordMember member = null)
        {
            if (member == null) {
                member = ctx.Member;
            }

            await ctx.RespondAsync(embed: buildUserInfoEmbed(member));
        }

        [Command("getid"), Description("Gets your ID and sends it to you in a DM")]
        public async Task GetId(CommandContext ctx)
        {
            await ctx.Member.SendMessageAsync($"Your Discord ID is: `{ctx.Member.Id}`");
        }

        private DiscordEmbed buildUserInfoEmbed(DiscordMember member)
        {
            var embed = new DiscordEmbedBuilder {
                Color = DiscordColor.Teal,
                ThumbnailUrl = member.AvatarUrl,
                Timestamp = DateTime.UtcNow
            };

            string name = string.Format(
                "{0}#{1}{2}",
                member.DisplayName,
                member.Discriminator,
                (member.IsBot) ? " (Bot)" : ""
            );

            embed.AddField("Name", name);
            string nickname = "No nickname given";

            if (member.Nickname != null) {
                nickname = member.Nickname;
            }

            embed.AddField("Nickname", nickname);
            embed.AddField("ID", member.Id.ToString());

            DefaultDateTimeHumanizeStrategy s = new DefaultDateTimeHumanizeStrategy();
            var joined = member.JoinedAt.DateTime;
            var h = s.Humanize(joined, DateTime.Now, CultureInfo.GetCultureInfoByIetfLanguageTag("en-US"));
            string joinedString = string.Format(
                "{0} {1} ({2})",
                joined.ToLongDateString(),
                joined.ToLongTimeString(),
                h
            );
            embed.AddField("Joined Server", joinedString);

            var rolesBuilder = new StringBuilder();

            foreach (var role in member.Roles) {
                rolesBuilder.Append(role.Name + ", ");
            }

            var rolesString = rolesBuilder.ToString().TrimEnd(new char[] { ',', ' ' });

            if (rolesString.Equals("")) {
                rolesString = "No roles assigned";
            }

            embed.AddField("Roles", rolesString);

            // Shit try/catch block to keep c# from
            // bitching about NullReferenceException because
            // fuck c#
            string presenceString = "";

            try {
                presenceString = member.Presence.Game.Name;
            } catch {
                presenceString = "";
            }

            if (!presenceString.Equals("")) {
                embed.AddField("Currently Playing", presenceString);
            }

            return embed;
        }
    }
}
