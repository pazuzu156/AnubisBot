using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using ServiceStack.OrmLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AnubisBot.Commands.User
{
    [Group("roles", CanInvokeWithoutSubcommand = true)]
    [Description("List server's joinable roles")]
    public class Roles : Utils.Roleable
    {
        public async Task ExecuteGroupAsync(CommandContext ctx)
        {
            string msg = "List of joinable roles:```";

            foreach (var role in getJoinableRoles(ctx.Guild)) {
                if (hasRole(ctx.Member, role)) {
                    msg += $"{role.Name.ToLower()} - You have this role\n";
                } else {
                    msg += $"{role.Name.ToLower()}\n";
                }
            }

            await ctx.Message.RespondAsync($"{msg}```");
        }

        [Command("restrict"), Description("Adds a role, or roles to a server's restriction list")]
        public async Task Restrict(CommandContext ctx, [RemainingText, Description("Role(s) to restrict")] string roleString)
        {
            if (CanManageRoles(ctx.Member)
                && roleString != "") {
                string[] rolesList = roleString.Split(new char[] { ',' });
                var restrictedRoles = getRestrictedRoles(ctx.Guild);

                foreach (var requestedRole in rolesList) {
                    try {
                        var role = getRoleFromName(ctx.Guild, requestedRole);

                        if (!isRoleRestricted(role)) {
                            var db = Sql.Database.Connect();
                            await db.InsertAsync(new Sql.RestrictedRoles {
                                guild_id = ctx.Guild.Id,
                                role_id = role.Id,
                                created_at = DateTime.Now,
                                updated_at = DateTime.Now
                            });
                            db.Close();
                        } else {
                            await ctx.RespondAsync($"{ctx.Member.Mention} the role {role.Name} is already restricted!");

                            return;
                        }
                    } catch {
                        await ctx.RespondAsync($"{ctx.Member.Mention} the role {requestedRole} doesn't exist");
                    }
                }

                if ((from s in rolesList select s).Count() == 1) {
                    var roleName = getRoleFromName(ctx.Guild, roleString).Name;
                    await ctx.RespondAsync($"{ctx.Member.Mention} Role {roleName} is now restricted!");
                } else {
                    string msg = "Roles: ";

                    foreach (var roleName in rolesList) {
                        var role = getRoleFromName(ctx.Guild, roleName);
                        msg += $"\"{role.Name}\", ";
                    }

                    msg = msg.TrimEnd(new char[] { ',', ' ' }) + " are now restricted!";

                    await ctx.RespondAsync($"{ctx.Member.Mention} {msg}");
                }
            }
        }

        [Command("unrestrict"), Description("Removes a role, or roles from the server's restriction list")]
        public async Task Unrestrict(CommandContext ctx, [RemainingText, Description("Role(s) to unrestrict")] string roleString)
        {
            if (CanManageRoles(ctx.Member)) {
                string[] rolesList = roleString.Split(new char[] { ',' });
                var restrictedRoles = getRestrictedRoles(ctx.Guild);

                foreach (var requestedRole in rolesList) {
                    var role = getRoleFromName(ctx.Guild, requestedRole);

                    if (isRoleRestricted(role)) {
                        var db = Sql.Database.Connect();
                        var rolesToRemove = await db.SelectAsync<Sql.RestrictedRoles>(
                            q => q.guild_id == ctx.Guild.Id &&
                            q.role_id == role.Id
                        );

                        foreach (var roleToRemove in rolesToRemove) {
                            await db.DeleteAsync(roleToRemove);
                        }
                    } else {
                        await ctx.RespondAsync($"{ctx.Member.Mention} that role is not restricted!");

                        return;
                    }
                }

                if ((from s in rolesList select s).Count() == 1) {
                    var roleName = getRoleFromName(ctx.Guild, roleString).Name;
                    await ctx.RespondAsync($"{ctx.Member.Mention} Role {roleName} is no longer restricted!");
                } else {
                    string msg = "Roles: ";

                    foreach (var roleName in rolesList) {
                        var role = getRoleFromName(ctx.Guild, roleName);
                        msg += $"\"{role.Name}\", ";
                    }

                    msg = msg.TrimEnd(new char[] { ',', ' ' }) + " are no longer restricted!";

                    await ctx.RespondAsync($"{ctx.Member.Mention} {msg}");
                }
            }
        }

        [Command("join"), Description("Join a role or list of roles")]
        public async Task Join(CommandContext ctx, [RemainingText, Description("Role(s) to join")] string roleString)
        {
            string[] rolesList = roleString.Split(new char[] { ',' });
            var restrictedRoles = getRestrictedRoles(ctx.Guild);
            var joinedRoles = new List<DiscordRole>();

            foreach (var requestedRole in rolesList) {
                var role = getRoleFromName(ctx.Guild, requestedRole);

                if (!isRoleRestricted(role)) {
                    if (!hasRole(ctx.Member, role)) {
                        await ctx.Member.GrantRoleAsync(role, $"{ctx.Member.DisplayName} has requested to join this role");
                        joinedRoles.Add(role);
                    } else {
                        await ctx.RespondAsync($"{ctx.Member.Mention} you're already in the role {role.Name}");
                    }
                } else {
                    await ctx.RespondAsync($"{ctx.Member.Mention} the role {role.Name} is restricted!");
                }
            }

            if (joinedRoles.Count == 1) {
                var roleName = joinedRoles[0].Name;
                await ctx.RespondAsync($"{ctx.Member.Mention} you have joined the role {roleName}");
            } else {
                string msg = "You have joined the roles: ";

                foreach (var role in joinedRoles) {
                    msg += $"\"{role.Name}\", ";
                }

                await ctx.RespondAsync($"{ctx.Member.Mention} {msg.TrimEnd(new char[] { ',', ' ' })}");
            }
        }

        [Command("leave"), Description("Leave a role or list of roles")]
        public async Task Leave(CommandContext ctx, [RemainingText, Description("Role(s) to leave")] string roleString)
        {
            string[] rolesList = roleString.Split(new char[] { ',' });
            var leftRoles = new List<DiscordRole>();

            foreach (var requestedRole in rolesList) {
                var role = getRoleFromName(ctx.Guild, requestedRole);

                if (hasRole(ctx.Member, role)) {
                    await ctx.Member.RevokeRoleAsync(role, $"{ctx.Member.DisplayName} has requested to leave this role");
                    leftRoles.Add(role);
                } else {
                    await ctx.RespondAsync($"{ctx.Member.Mention} you do not have the role {role.Name}");
                }
            }

            if (leftRoles.Count == 1) {
                var roleName = leftRoles[0].Name;
                await ctx.RespondAsync($"{ctx.Member.Mention} you have left the role {roleName}");
            } else {
                string msg = "You have left the roles: ";

                foreach (var role in leftRoles) {
                    msg += $"\"{role.Name}\", ";
                }

                await ctx.RespondAsync($"{ctx.Member.Mention} {msg.TrimEnd(new char[] { ',', ' ' })}");
            }
        }
    }
}
