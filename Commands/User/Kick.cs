using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using System.Threading.Tasks;

namespace AnubisBot.Commands.User
{
    public class Kick : BaseCommand
    {
        [Command("kick"), Description("Kicks a user [Requires KickMember persission]")]
        public async Task KickUser(CommandContext ctx, DiscordMember member, [RemainingText] string reason = "")
        {
            reason = reason.Trim(new char[0]);

            if (reason == "") {
                reason = "None Given";
            }

            if (CanKickMembers(ctx.Member)) {
                var kicker = ctx.Member.DisplayName;
                var msg = $"{ctx.Member.DisplayName} kicked you. | Reason: {reason}";

                var embed = new DiscordEmbedBuilder {
                    Title = $"User {member.DisplayName} was kicked",
                    Color = DiscordColor.Goldenrod
                };
                embed.AddField("Kicked By", kicker, true);
                embed.AddField("Reason", reason, true);

                await member.SendMessageAsync(msg);
                await member.RemoveAsync(reason);
                await BotClient.GetBotSpam(ctx.Guild).SendMessageAsync(embed: embed);
            }
        }
    }
}
