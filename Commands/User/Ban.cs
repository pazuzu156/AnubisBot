using DSharpPlus.Entities;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using System.Threading.Tasks;

namespace AnubisBot.Commands.User
{
    public class Ban : BaseCommand
    {
        [Command("ban"), Description("Bans a user [Requires BanMember permission]")]
        public async Task BanUser(CommandContext ctx, DiscordMember member, [RemainingText] string reason = "")
        {
            reason = reason.Trim(new char[0]);

            if (reason == "") {
                reason = "None Given";
            }

            if (CanBanMembers(ctx.Member)) {
                var banner = ctx.Member.DisplayName;
                var msg = $"{ctx.Member.DisplayName} banned you. | Reason: {reason}";

                var embed = new DiscordEmbedBuilder {
                    Title = $"User {member.DisplayName} was banned",
                    Color = DiscordColor.Red
                };
                embed.AddField("Banned By", banner, true);
                embed.AddField("Reason", reason, true);

                await member.SendMessageAsync(msg);
                await member.BanAsync(7, reason);
                await BotClient.GetBotSpam(ctx.Guild).SendMessageAsync(embed: embed);
            }
        }
    }
}
