using System;
using AnubisBot.Data;
using AnubisBot.Utils;
using DSharpPlus.CommandsNext;
using DSharpPlus.Entities;
using ServiceStack.OrmLite;

namespace AnubisBot.Commands
{
    public class BaseCommand
    {
        protected JsonData Config { get; private set; }

        public BaseCommand()
        {
            var s = Settings.getInstance();
            Config = s.config;
        }

        /// <summary>
        /// Checks to see of a user has a specified permission.
        /// </summary>
        /// <param name="member">The member to check</param>
        /// <param name="permission">The permission to check for the given member</param>
        /// <returns>System.Boolean</returns>
        protected bool HasPermission(DiscordMember member, DSharpPlus.Permissions permission)
        {
            return Utils.Permissions.Can(member, permission);
        }

        /// <summary>
        /// Checks if a user can manage a guild.
        /// </summary>
        /// <param name="member">The memeber to check</param>
        /// <returns>System.Boolean</returns>
        protected bool CanManageGuild(DiscordMember member)
        {
            return HasPermission(member, DSharpPlus.Permissions.ManageGuild);
        }

        /// <summary>
        /// Checks if a user can manage messages.
        /// </summary>
        /// <param name="member">The member to check</param>
        /// <returns>System.Boolean</returns>
        protected bool CanManageMessages(DiscordMember member)
        {
            return HasPermission(member, DSharpPlus.Permissions.ManageMessages);
        }

        /// <summary>
        /// Checks if a user can kick other users.
        /// </summary>
        /// <param name="member">The member to check</param>
        /// <returns>System.Boolean</returns>
        protected bool CanKickMembers(DiscordMember member)
        {
            return HasPermission(member, DSharpPlus.Permissions.KickMembers);
        }

        /// <summary>
        /// Checks if a user can ban other users.
        /// </summary>
        /// <param name="member">The member to check</param>
        /// <returns>System.Boolean</returns>
        protected bool CanBanMembers(DiscordMember member)
        {
            return HasPermission(member, DSharpPlus.Permissions.BanMembers);
        }

        /// <summary>
        /// Checks if a user can manage guild roles.
        /// </summary>
        /// <param name="member">The member to check</param>
        /// <returns>System.Boolean</returns>
        protected bool CanManageRoles(DiscordMember member)
        {
            return HasPermission(member, DSharpPlus.Permissions.ManageRoles);
        }

        protected DiscordChannel GetBotSpam(CommandContext ctx)
        {
            var guild = ctx.Guild;

            try {
                var db = Sql.Database.Connect();
                var bs = db.SingleAsync<Sql.BotSpam>(q => q.guild_id == guild.Id).Result;

                if (bs != null) {
                    return guild.GetChannel(bs.channel_id);
                }

                return ctx.Channel;
            } catch (Exception) {
                return ctx.Channel;
            }
        }
    }
}
