// using System;
// using System.Diagnostics;
// using System.IO;
// using System.Runtime.InteropServices;
// using System.Threading;
// using System.Threading.Tasks;
// using DSharpPlus.CommandsNext;
// using DSharpPlus.CommandsNext.Attributes;
// using DSharpPlus.VoiceNext;
// using Google.Apis.Services;
// using Google.Apis.YouTube.v3;

// /*
//     Please do not use this command, it's broken, and causes
//     many issues with the bot and any server it's running on.

//     I do not plan on fixing this, as I feel it's a completely pointless
//     command, since there's so many bots out there that do this way better.

//     If you feel like fixing it yourself, by all means, go for it. I'm leaving
//     it as is, and the code here for any to fuck around with.
//  */

// namespace AnubisBot.Commands
// {
//     [Group("yt"), Description("YouTube commands")]
//     public class YouTube : BaseCommand
//     {
//         private string m_VideoFileName;

//         private Process m_ffmpeg;

//         [Command("play")]
//         [Description("Search YouTube for a video to listen to and play it")]
//         public async Task Play(CommandContext ctx, [RemainingText, Description("Search for a song to listen to")] string query)
//         {
//             var yts = new YouTubeService(new BaseClientService.Initializer() {
//                 ApiKey = Config.YouTube.Token,
//                 ApplicationName = GetType().ToString(),
//             });

//             var searchListRequest = yts.Search.List("snippet");
//             searchListRequest.Q = query;
//             searchListRequest.MaxResults = 1;
//             var searchListResponse = await searchListRequest.ExecuteAsync();
//             var v = searchListResponse.Items[0];

//             await ctx.RespondAsync("Downloading track...");

//             if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
//                 m_VideoFileName = $@"/tmp/anubisbot/audio/{v.Id.VideoId}-{ctx.Guild.Id}.mp3";
//             } else {
//                 m_VideoFileName = $@"audio/{v.Id.VideoId}-{ctx.Guild.Id}.mp3";
//             }

//             downloadTrack(v.Id.VideoId);

//             var vnc = await connect(ctx);
//             await vnc.SendSpeakingAsync(true);

//             await ctx.RespondAsync($"Listening to song: {v.Snippet.Title}");

//             ProcessStartInfo ffpsi = new ProcessStartInfo() {
//                 FileName = "ffmpeg",
//                 Arguments = $@"-i ""{m_VideoFileName}"" -ac 2 -f s16le -ar 48000 pipe:1",
//                 RedirectStandardOutput = true,
//                 UseShellExecute = false
//             };
//             m_ffmpeg = Process.Start(ffpsi);

//             var ffout = m_ffmpeg.StandardOutput.BaseStream;
//             var buff = new byte[3840];
//             var br = 0;

//             while ((br = ffout.Read(buff, 0, buff.Length)) > 0) {
//                 if (br < buff.Length) {
//                     for (var i = br; i < buff.Length; i++) {
//                         buff[i] = 0;
//                     }
//                 }

//                 await vnc.SendAsync(buff, 20);
//             }

//             await vnc.SendSpeakingAsync(false);

//             m_ffmpeg.Kill();
//             Thread.Sleep(1000); // Sleep for a second before attempting to delete file
//             File.Delete(m_VideoFileName);
//         }

//         [Command("url"), Description("Play a video from a YouTube URL")]
//         public async Task URL(CommandContext ctx, [RemainingText, Description("The URL of the video to play")] string url)
//         {
//             await ctx.RespondAsync("Obtaining tack info...");

//             ProcessStartInfo ytdl_info_psi = new ProcessStartInfo() {
//                 FileName = "youtube-dl",
//                 Arguments = $"--get-title --get-id {url}",
//                 RedirectStandardOutput = true,
//                 UseShellExecute = false
//             };

//             Process ytdlp = Process.Start(ytdl_info_psi);
//             ytdlp.WaitForExit();

//             var reader = await ytdlp.StandardOutput.ReadToEndAsync();
//             var info = reader.Trim().ToString().Split(new string[] { "\n" }, StringSplitOptions.None);

//             if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux)) {
//                 m_VideoFileName = $@"/tmp/anubisbot/audio/{info[1]}-{ctx.Guild.Id}.mp3";
//             } else {
//                 m_VideoFileName = $@"audio/{info[1]}-{ctx.Guild.Id}.mp3";
//             }

//             await ctx.RespondAsync("Downloading song...");
//             downloadTrack(info[1]);

//             await ctx.Message.DeleteAsync();

//             var vnc = await connect(ctx);
//             await vnc.SendSpeakingAsync(true);
//             await ctx.RespondAsync($"Listening to song: {info[0]}");
//             await playTrack(vnc);
//         }

//         [Command("stop"), Description("Stops the current playing song")]
//         public async Task Stop(CommandContext ctx)
//         {
//             await ctx.RespondAsync("Stopping song...");

//             var vnext = ctx.Client.GetVoiceNextClient();
//             var vnc = vnext.GetConnection(ctx.Guild);

//             if (vnc == null) {
//                 await ctx.RespondAsync($"{ctx.Member.Mention}, I'm not playing anything at the moment");

//                 return;
//             }

//             await vnc.SendSpeakingAsync(false);
//             vnc.Disconnect();
//             m_ffmpeg.Kill();
//             Thread.Sleep(1000); // Sleep for a second before attempting to delete file
//             File.Delete(m_VideoFileName);
//         }

//         private async Task<VoiceNextConnection> connect(CommandContext ctx)
//         {
//             var vnext = ctx.Client.GetVoiceNextClient();
//             var vnc = vnext.GetConnection(ctx.Guild);

//             if (vnc != null) {
//                 await ctx.RespondAsync($"{ctx.Member.Mention}, I'm already connected to a voice channel here");

//                 return null;
//             }

//             var chn = ctx.Member?.VoiceState?.Channel;

//             if (chn == null) {
//                 await ctx.RespondAsync($"{ctx.Member.Mention}, You need to be in a voice channel to invoke this command!");

//                 return null;
//             }

//             vnc = await vnext.ConnectAsync(chn);

//             return vnc;
//         }

//         private void downloadTrack(string id)
//         {
//             ProcessStartInfo ytpsi = new ProcessStartInfo() {
//                 FileName = "youtube-dl",
//                 Arguments = $@"-x --audio-format mp3 -o ""{m_VideoFileName}"" https://youtube.com/watch?v={id}",
//                 CreateNoWindow = true,
//                 RedirectStandardOutput = true
//             };

//             Process ytdl = Process.Start(ytpsi);
//             ytdl.WaitForExit();
//         }

//         private async Task playTrack(VoiceNextConnection vnc)
//         {
//             ProcessStartInfo ffpsi = null;

//             // dirty, but effective. Will find something better at some point
//             if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows)) {
//                 ffpsi = new ProcessStartInfo() {
//                     FileName = "ffmpeg",
//                     Arguments = $@"-i ""{m_VideoFileName}"" -ac 2 -f s16le -ar 48000 pipe:1",
//                     RedirectStandardOutput = true,
//                     UseShellExecute = false // Works fine on Windows (maybe? Could it actually have the same issue Linux has?)
//                 };
//             } else {
//                 ffpsi = new ProcessStartInfo() {
//                     FileName = "ffmpeg",
//                     Arguments = $@"-i ""{m_VideoFileName}"" -ac 2 -f s16le -ar 48000 pipe:1",
//                     RedirectStandardOutput = true
//                 };
//             }

//             m_ffmpeg = Process.Start(ffpsi);

//             var ffout = m_ffmpeg.StandardOutput.BaseStream;
//             var buff = new byte[3840];
//             var br = 0;

//             while ((br = ffout.Read(buff, 0, buff.Length)) > 0) {
//                 if (br < buff.Length) {
//                     for (var i = br; i < buff.Length; i++) {
//                         buff[i] = 0;
//                     }
//                 }

//                 await vnc.SendAsync(buff, 20);
//             }

//             await vnc.SendSpeakingAsync(false);

//             m_ffmpeg.Kill();
//             Thread.Sleep(1000); // Sleep for a second before attempting to delete file
//             File.Delete(m_VideoFileName);
//         }
//     }
// }
