using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using System.Linq;
using System.Threading.Tasks;

namespace AnubisBot.Commands
{
    [Group("prune", CanInvokeWithoutSubcommand = true)]
    [Description("Prune messages [Requires ManageMessages permission]")]
    class Prune : BaseCommand
    {
        /// <summary>
        /// Default message history limit.
        /// </summary>
        private const int _defaultLimit = 50;

        public async Task ExecuteGroupAsync(CommandContext ctx, [RemainingText, Description("Number of messages to remove")] int limit)
        {
            limit = (limit == 0) ? _defaultLimit : limit;

            if (CanManageMessages(ctx.Member)) {
                var channel = ctx.Channel;
                var messages = await channel.GetMessagesAsync(limit);

                await ctx.Channel.DeleteMessagesAsync(messages);

                var l = (limit == 1) ? "message" : "messages";
                await ctx.RespondAsync($"{ctx.Member.Mention} Pruned {messages.Count} out of {limit} total requested {l}");
            } else {
                await ctx.Message.RespondAsync($"{ctx.Member.Mention}, you do not have permission to prune messages!");
            }
        }

        [Command("user")]
        [Description("Prune user messages")]
        public async Task PruneUser(CommandContext ctx, [Description("Member to prune")] DiscordMember member, [RemainingText, Description("Number of messages to remove")] int limit)
        {
            limit = (limit == 0) ? _defaultLimit : limit;

            if (CanManageMessages(ctx.Member)) {
                var messages = await ctx.Channel.GetMessagesAsync(limit);
                var messagesToDelete = messages.Where(q => q.Author == ctx.User).ToList();
                await ctx.Channel.DeleteMessagesAsync(messagesToDelete);

                var l = (limit == 1) ? "message" : "messages";
                await ctx.RespondAsync($"{ctx.Member.Mention} Pruned {messagesToDelete.Count} out of {limit} total requested {l}");
            } else {
                await ctx.Message.RespondAsync($"{ctx.Member.Mention}, you do not have permission to prune messages!");
            }
        }

        [Command("bot")]
        [Description("Prune bot messages")]
        public async Task PruneBot(CommandContext ctx, [RemainingText, Description("Number of messages to remove")] int limit)
        {
            limit = (limit == 0) ? _defaultLimit : limit;

            if (CanManageMessages(ctx.Member)) {
                var messages = await ctx.Channel.GetMessagesAsync(limit);
                var messagesToDelete = messages.Where(q => q.Author == ctx.Client.CurrentUser).ToList();
                await ctx.Channel.DeleteMessagesAsync(messagesToDelete);

                var l = (limit == 1) ? "message" : "messages";
                await ctx.RespondAsync($"{ctx.Member.Mention} Pruned {messagesToDelete.Count} out of {limit} total requested {l}");
            } else {
                await ctx.Message.RespondAsync($"{ctx.Member.Mention}, you do not have permission to prune messages!");
            }
        }
    }
}
