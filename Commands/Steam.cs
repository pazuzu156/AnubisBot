using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using AnubisBot.Data;
using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AnubisBot.Commands
{
    [Group("steam", CanInvokeWithoutSubcommand = false), Description("Gets info using SteamAPI")]
    public class Steam : BaseCommand
    {
        [Command("discount")]
        [Description("Checks for discount on a given app (uses AppID for game)")]
        public async Task GetAppDiscount(CommandContext ctx,
            [Description("The App ID of the game/app currently on sale")] uint appId,
            [Description("When the sale ends: HH-MM-SS (2 days is 48 hours)")] string saleEnd,
            [RemainingText, Description("Channel to send message to")] DiscordChannel channel = null)
        {
            if (channel == null) {
                channel = GetBotSpam(ctx);
            }

            HttpWebRequest req = (HttpWebRequest)WebRequest.Create($"https://store.steampowered.com/api/appdetails/?appids={appId}");

            using (var reader = new StreamReader(req.GetResponse().GetResponseStream())) {
                var ad = (JObject)JsonConvert.DeserializeObject(reader.ReadToEnd());
                var app = (JObject)ad.Property($"{appId}").Value;
                var data = JsonConvert.DeserializeObject<StoreAppData>(app.Property("data").Value.ToString());

                if (data.IsFree) {
                    await ctx.RespondAsync($"{data.Name} is free, no discounts to obtain");
                } else {
                    var price = data.PriceOverview;

                    if (price.DiscountPercent > 0) {
                        DiscordColor color = DiscordColor.None;

                        switch (price.DiscountPercent) {
                            case int n when (n < 25):
                                color = DiscordColor.Red;
                                break;
                            case int n when (n > 25 && n < 65):
                                color = DiscordColor.Yellow;
                                break;
                            case int n when (n >= 65):
                                color = DiscordColor.Green;
                                break;
                        }

                        var embed = new DiscordEmbedBuilder {
                            Color = color,
                            ThumbnailUrl = data.HeaderImage,
                            Timestamp = DateTime.UtcNow
                        };

                        embed.AddField("Name", data.Name);
                        embed.AddField("Price", $"~~{price.InitialFormatted}~~ {price.FinalFormatted} ({price.DiscountPercent}% Off)");

                        var thenOrig = DateTime.UtcNow;
                        var now = DateTime.UtcNow;
                        var then = DateTime.UtcNow;
                        var i = saleEnd.Split('-');

                        try {
                            then = thenOrig.AddHours(double.Parse(i[0]))
                                .AddMinutes(double.Parse(i[1]))
                                .AddSeconds(double.Parse(i[2]));
                        } catch (Exception) {
                            await ctx.RespondAsync("Invalid Sale End syntax");

                            return;
                        }

                        var t = then.Subtract(now);
                        IList<string> strList = new List<string>();
                        string str = "";

                        if (t.Days > 0) {
                            var s = (t.Days == 1) ? "" : "s";
                            strList.Add($"{t.Days} Day{s}");
                        }
                        if (t.Hours > 0) {
                            var s = (t.Hours == 1) ? "" : "s";
                            strList.Add($"{t.Hours} Hour{s}");
                        }
                        if (t.Minutes > 0) {
                            var s = (t.Minutes == 1) ? "" : "s";
                            strList.Add($"{t.Minutes} Minute{s}");
                        }

                        foreach (var s in strList) {
                            str += $"{s}, ";
                        }

                        embed.AddField("Sale End", $"{then.ToLongDateString()} {then.ToLongTimeString()} ({str.TrimEnd(new char[] {',', ' '})} from now)");
                        embed.AddField("Store Page", $"https://store.steampowered.com/app/{data.AppId}");

                        try {
                            await channel.SendMessageAsync(embed: embed);
                        } catch (Exception ex) {
                            await ctx.RespondAsync(ex.Message);
                        }
                    } else {
                        await ctx.RespondAsync($"{data.Name} is currently at full price");
                    }
                }
            }

            await ctx.Message.DeleteAsync().ContinueWith((task) => {
                ctx.RespondAsync($"Discount has been posted in #{channel.Name}");
            });
        }

        [Command("expire")]
        [Description("Sets a given discount as expired (Must be used in channel embed is in)")]
        public async Task Expire(CommandContext ctx,
            [Description("Message ID of the embed to expire")] ulong messageId,
            [RemainingText, Description("Channel where the message can be found")] DiscordChannel channel = null)
        {
            DiscordMessage message = null;

            if (channel == null) {
                message = ctx.Channel.GetMessageAsync(messageId).Result;
            } else {
                message = channel.GetMessageAsync(messageId).Result;
            }

            DiscordEmbed embed = message.Embeds[0];
            DiscordEmbedThumbnail thumbnail = embed.Thumbnail;
            string appName = "";
            string storePageUrl = "";

            foreach (var field in embed.Fields) {
                switch (field.Name) {
                    case "Name":
                        appName = field.Value;
                        break;
                    case "Store Page":
                        storePageUrl = field.Value;
                        break;
                }
            }

            var newEmbed = new DiscordEmbedBuilder {
                ThumbnailUrl = thumbnail.Url.ToString(),
                Color = DiscordColor.Gray
            };

            newEmbed.AddField("Name", appName);
            newEmbed.AddField("Sale End", $"Sale of {appName} has ended");
            newEmbed.AddField("Store Page", storePageUrl);

            await ctx.Message.DeleteAsync().ContinueWith((t) => {
                message.ModifyAsync(embed: newEmbed).ContinueWith((t2) => {
                    if (channel == null) {
                        channel = ctx.Channel;
                    }

                    ctx.RespondAsync($"Discounted game was set to expired in channel {channel.Name}");
                });
            });
        }

        [Command("getid"), Description("Returns the given user's Steam64 ID")]
        public async Task GetSteam64Id(CommandContext ctx,
            [RemainingText, Description("Steam user's username")] string username)
        {
            WebRequest request = WebRequest.Create(new Uri("https://api.kalebklein.com/steam/public/getid?username=" + username));
            WebResponse response = request.GetResponse();
            SteamInfo info = null;

            using (var reader = new StreamReader(response.GetResponseStream())) {
                info = JsonConvert.DeserializeObject<SteamInfo>(reader.ReadToEnd());
            }

            if (info.error) {
                await ctx.Message.RespondAsync($"{ctx.Member.Mention} Error: {info.message}");
            } else {
                await ctx.Message.RespondAsync($"{ctx.Member.Mention} {username}'s Steam64 ID is: {info.steam64}");
            }
        }

        private class SteamInfo
        {
            public bool error { get; set; }
            public string steam64 { get; set; }
            public string message { get; set; }
        }
    }
}
