using DSharpPlus.CommandsNext;
using DSharpPlus.CommandsNext.Attributes;
using DSharpPlus.Entities;
using ServiceStack.OrmLite;
using System;
using System.Threading.Tasks;

namespace AnubisBot.Commands
{
    [Group("botmod"), Description("Sets bot automated stuff [Requires ManageGuild permission]")]
    public class BotMod : BaseCommand
    {
        [Command("setbotspam")]
        [Description("Sets the default bot spam channel")]
        public async Task SetBotSpam(CommandContext ctx,
            [RemainingText, Description("The channel the bot should send automated messages to")] DiscordChannel channel)
        {
            if (CanManageGuild(ctx.Member)) {
                var db = Sql.Database.Connect();
                var guild = ctx.Guild;
                var bs = await db.SingleAsync<Sql.BotSpam>(q => q.guild_id == guild.Id);

                if (bs != null) {
                    bs.channel_id = channel.Id;
                    bs.updated_at = DateTime.Now;

                    await db.UpdateAsync(bs);
                } else {
                    await db.InsertAsync(new Sql.BotSpam {
                        guild_id = guild.Id,
                        channel_id = channel.Id,
                        is_enabled = true,
                        join_message = "",
                        leave_message = "",
                        created_at = DateTime.Now,
                        updated_at = DateTime.Now
                    });
                }

                db.Close();

                await ctx.Message.RespondAsync($"{ctx.Member.Mention} Bot spam channel changed to: #{channel.Name} (*{channel.Id}*)");
            } else {
                await ctx.Message.RespondAsync($"{ctx.Member.Mention}, you do not have permission to use this command!");
            }
        }

        [Command("enablemessages"), Description("Enable/Disable member join/leave messages")]
        public async Task EnableMessages(CommandContext ctx, bool enable)
        {
            if (CanManageGuild(ctx.Member)) {
                string msg = "";

                if (enable) {
                    msg = "Join/Leave messages have been enabled";
                } else {
                    msg = "Join/Leave messages have been disabled";
                }

                var db = Sql.Database.Connect();
                var bs = await db.SingleAsync<Sql.BotSpam>(q => q.guild_id == ctx.Guild.Id);

                if (bs != null) {
                    bs.is_enabled = enable;
                    bs.updated_at = DateTime.Now;

                    await db.UpdateAsync(bs);
                    await ctx.RespondAsync(msg);

                    db.Close();
                } else {
                    await ctx.RespondAsync("No bot spam channel has been set, messages cannot be enabled or disabled!");
                }
            }
        }

        [Command("joinmsg"), Description("Sets the message displayed when a user joined the server")]
        public async Task JoinMessage(CommandContext ctx, [RemainingText, Description("The message to display")] string message)
        {
            if (CanManageGuild(ctx.Member)) {
                var db = Sql.Database.Connect();
                var bs = await db.SingleAsync<Sql.BotSpam>(q => q.guild_id == ctx.Guild.Id);

                if (bs == null) {
                    await ctx.RespondAsync($"{ctx.Member.Mention} no bot spam channel has been set!");

                    return;
                }

                bs.join_message = Uri.EscapeDataString(message);
                bs.updated_at = DateTime.Now;

                await db.UpdateAsync(bs);
                await ctx.RespondAsync($"{ctx.Member.Mention} Welcome message now set!");

                db.Close();
            }
        }

        [Command("leavemsg"), Description("Sets the message displayed when a user leaves the server")]
        public async Task LeaveMessage(CommandContext ctx, [RemainingText, Description("The message to display")] string message)
        {
            if (CanManageGuild(ctx.Member)) {
                var db = Sql.Database.Connect();
                var bs = await db.SingleAsync<Sql.BotSpam>(q => q.guild_id == ctx.Guild.Id);

                if (bs == null) {
                    await ctx.RespondAsync($"{ctx.Member.Mention} no bot spam channel has been set!");

                    return;
                }

                bs.leave_message = Uri.EscapeDataString(message);
                bs.updated_at = DateTime.Now;

                await db.UpdateAsync(bs);
                await ctx.RespondAsync($"{ctx.Member.Mention} Dismissal message now set!");

                db.Close();
            }
        }

        [Command("deljoinmsg"), Description("Resets the welcome message to the default")]
        public async Task DeleteJoinMessage(CommandContext ctx)
        {
            if (CanManageGuild(ctx.Member)) {
                var db = Sql.Database.Connect();
                var bs = await db.SingleAsync<Sql.BotSpam>(q => q.guild_id == ctx.Guild.Id);

                if (bs == null) {
                    await ctx.RespondAsync($"{ctx.Member.Mention} no bot spam channel was ever set!");

                    return;
                }

                bs.join_message = "";
                bs.updated_at = DateTime.Now;

                await db.UpdateAsync(bs);
                await ctx.RespondAsync($"{ctx.Member.Mention} Custom welcome message removed and reset to the default!");

                db.Close();
            }
        }

        [Command("delleavemsg"), Description("Resets the dismissal message to the default")]
        public async Task DeleteLeaveMessage(CommandContext ctx)
        {
            if (CanManageGuild(ctx.Member)) {
                var db = Sql.Database.Connect();
                var bs = await db.SingleAsync<Sql.BotSpam>(q => q.guild_id == ctx.Guild.Id);

                if (bs == null) {
                    await ctx.RespondAsync($"{ctx.Member.Mention} no bot spam channel was ever set!");

                    return;
                }

                bs.leave_message = "";
                bs.updated_at = DateTime.Now;

                await db.UpdateAsync(bs);
                await ctx.RespondAsync($"{ctx.Member.Mention} Custom dismissal message removed and reset to the default!");

                db.Close();
            }
        }
    }
}
