.PHONY: build

all: build

build:
	BFM=true ./build

clean:
	./build -c

install:
	sudo tar -xvf AnubisBot.tgz -C /opt/
	sudo chmod +x /opt/AnubisBot/AnubisBot
	sudo ln -sf /opt/AnubisBot/AnubisBot /usr/bin/anubisbot
	sudo install -D -m755 anubisbot.service /usr/lib/systemd/system/anubisbot.service
	sudo mkdir -p /etc/AnubisBot/

uninstall:
	sudo rm -rf /opt/AnubisBot
	sudo rm -rf /usr/bin/anubisbot
	sudo systemctl disable --now anubisbot
	sudo rm -rf /usr/lib/systemd/system/anubisbot.service
