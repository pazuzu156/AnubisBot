using AnubisBot.Data;
using AnubisBot.Utils;
using DSharpPlus;
using DSharpPlus.CommandsNext;
using System;
using System.Collections.Generic;
using System.Reflection;
using System.Threading.Tasks;

namespace AnubisBot
{
    /// <summary>
    /// Bot entry point class
    /// </summary>
    public class Program
    {
        /// <summary>
        /// Bot's Version.
        /// </summary>
        public static Version Version { get; private set; }

        /// <summary>
        /// Bot's command prefix.
        /// </summary>
        public static string Prefix { get; private set; }

        /// <summary>
        /// App starting timestamp
        /// </summary>
        public static DateTime StartTime { get; private set; }

        /// <summary>
        /// Discord client instance.
        /// </summary>
        private DiscordShardedClient Client;

        /// <summary>
        /// Commands module instance.
        /// </summary>
        private IReadOnlyDictionary<int, CommandsNextModule> Commands;

        /// <summary>
        /// /// Log tag for logger.
        /// </summary>
        public const string LOG_TAG = "AnubisBot";

        /// <summary>
        /// Constructor.
        /// </summary>
        /// <param name="args">System arguments</param>
        public static void Main(string[] args)
        {
            // get bot version first
            Version = Assembly.GetExecutingAssembly().GetName().Version;
            StartTime = DateTime.Now;

            var s = Settings.getInstance();
            var json = s.config;

            if (json.Token.Equals(string.Empty)) {
                if (s.Generated) {
                    Console.WriteLine("Bot config file has been generated, open \"config.json\" and add in your bot token.");
                } else {
                    Console.WriteLine("You forgot to insert your bot token! Please add that in \"config.json\"");
                }

                Console.Write("Press any key to exit...");
                Console.ReadKey();

                return;
            }

            new Program().RunBotAsync(json).GetAwaiter().GetResult();
        }

        /// <summary>
        /// Runs the bot
        /// </summary>
        public async Task RunBotAsync(JsonData json)
        {
            // Add to this list of custom commands, namespace and all
            var cmdList = json.CommandList;

            if (json.PrefixSpace == true) {
                Prefix = json.Prefix + " ";
            } else {
                Prefix = json.Prefix;
            }

            Client = new DiscordShardedClient(new DiscordConfiguration {
                Token = json.Token,
                TokenType = TokenType.Bot,
                AutoReconnect = true,
                LogLevel = json.LogLevel,
                UseInternalLogHandler = json.UseInternalLogHandler,
                ShardCount = json.ShardCount,
            });

            var botClient = new BotClient(Client);

            Commands = Client.UseCommandsNext(new CommandsNextConfiguration {
                StringPrefix = Prefix
            });

            foreach (var command in Commands.Values) {
                command.CommandExecuted += botClient.Commands_CommandExecuted;

                foreach (var cmd in cmdList) {
                    try {
                        Type cmdType = Type.GetType(cmd);
                        command.RegisterCommands(cmdType);
                    } catch (Exception ex) {
                        Console.WriteLine($"Error attempting to register command: {cmd}");
                        Console.WriteLine($"Exception: {ex.Message}");

                        return;
                    }
                }
            }

            // VoiceNext is dead for this bot, since I killed the
            // YouTube command.

            // foreach (DiscordClient c in Client.ShardClients.Values) {
            //     c.UseVoiceNext();
            // }

            await Client.StartAsync();
            await Task.Delay(-1);
        }
    }
}
