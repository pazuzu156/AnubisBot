using DSharpPlus;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace AnubisBot.Data
{
    public struct JsonData
    {
        [JsonProperty("token")]
        public string Token { get; set; }

        [JsonProperty("prefix")]
        public string Prefix { get; set; }

        [JsonProperty("prefixSpace")]
        public bool PrefixSpace { get; set; }

        [JsonProperty("useInternalLogHandler")]
        public bool UseInternalLogHandler { get; set; }

        [JsonProperty("logLevel")]
        public LogLevel LogLevel { get; set; }

        [JsonProperty("botPresence")]
        public string[] PresenceStrings { get; set; }

        [JsonProperty("shardCount")]
        public int ShardCount { get; set; }

        [JsonProperty("database")]
        public DatabaseInfo Database { get; set; }

        [JsonProperty("lastfm")]
        public LastFM LastFM { get; set; }

        [JsonProperty("discordBots")]
        public DiscordBots DiscordBots { get; set; }

        [JsonProperty("youtube")]
        public YouTube YouTube { get; set; }

        [JsonProperty("commands")]
        public List<string> CommandList { get; set; }
    }

    [JsonObject]
    public class BotPresence
    {
        [JsonProperty("interval")]
        public int Interval { get; set; }

        [JsonProperty("strings")]
        public string[] Strings { get; set; }
    }

    [JsonObject]
    public class DatabaseInfo
    {
        [JsonProperty("hostname")]
        public string Host { get; set; }

        [JsonProperty("port")]
        public int Port { get; set; }

        [JsonProperty("username")]
        public string Username { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("dbname")]
        public string DatabaseName { get; set; }
    }

    [JsonObject]
    public class LastFM
    {
        [JsonProperty("apikey")]
        public string ApiKey { get; set; }
        [JsonProperty("secret")]
        public string Secret { get; set; }
    }

    [JsonObject]
    public class DiscordBots
    {
        [JsonProperty("apikey")]
        public string ApiKey { get; set; }
        [JsonProperty("announce")]
        public bool Announce { get; set; }
    }

    [JsonObject]
    public class YouTube
    {
        [JsonProperty("api_token")]
        public string Token { get; set; }
    }
}
