using Newtonsoft.Json;

namespace AnubisBot.Data
{
    [JsonObject]
    internal class StoreAppData
    {
        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("steam_appid")]
        public uint AppId { get; set; }

        [JsonProperty("is_free")]
        public bool IsFree { get; set; }

        [JsonProperty("header_image")]
        public string HeaderImage { get; set; }

        [JsonProperty("price_overview")]
        public PriceOverview PriceOverview { get; set; }
    }

    [JsonObject]
    internal class PriceOverview
    {
        [JsonProperty("currency")]
        public string Currency { get; set; }

        [JsonProperty("initial")]
        public uint Initial { get; set; }

        [JsonProperty("final")]
        public uint Final { get; set; }

        [JsonProperty("discount_percent")]
        public int DiscountPercent { get; set; }

        [JsonProperty("initial_formatted")]
        public string InitialFormatted { get; set; }

        [JsonProperty("final_formatted")]
        public string FinalFormatted { get; set; }
    }
}
