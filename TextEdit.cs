using System;
using System.Text;

namespace AnubisBot
{
    public static class TextEdit
    {
        /// <summary>
        /// Normalizes text
        /// </summary>
        /// <param name="text">Text to normalize</param>
        /// <returns>System.String</returns>
        public static string Normalize(string text)
        {
            var wrappedText = WrapText(text);
            string[] lines = wrappedText.Split(new string[] { "\n" }, StringSplitOptions.None);
            var maxlen = lines[0].Length;

            for (int i = 0; i < lines.Length; i++) {
                var count = lines[i].Split(new string[] { "" }, StringSplitOptions.None).Length;
                var spaceCount = maxlen - count;
                var append = string.Empty;

                for (var j = 0; j < spaceCount; j++) {
                    append += " ";
                }

                lines[i] = lines[i] + append;
            }

            var sb = new StringBuilder();

            foreach (var line in lines) {
                sb.Append(line + '\n');
            }

            return sb.ToString();
        }

        /// <summary>
        /// Performs a word wrap for text
        /// </summary>
        /// <param name="text">The text to perform the wrap on</param>
        /// <param name="wrapSize">Word count to start wrapping at</param>
        /// <returns>System.String</returns>
        public static string WrapText(string text, int wrapSize = 40)
        {
            int pos, next = 0;
            var sb = new StringBuilder();

            if (wrapSize < 1) {
                return text;
            }

            for (pos = 0; pos < text.Length; pos = next) {
                int eol = text.IndexOf(Environment.NewLine, pos);

                if (eol == -1) {
                    next = eol = text.Length;
                } else {
                    next = eol + Environment.NewLine.Length;
                }

                if (eol > pos) {
                    do {
                        int len = eol - pos;
                        if (len > wrapSize) {
                            len = BreakLine(text, pos, wrapSize);
                        }

                        sb.Append(text, pos, len);
                        sb.Append(Environment.NewLine);

                        pos += len;

                        while (pos < eol && Char.IsWhiteSpace(text[pos])) {
                            pos++;
                        }
                    } while (eol > pos);
                } else {
                    sb.Append(Environment.NewLine);
                }
            }

            return sb.ToString();
        }

        /// <summary>
        /// Initializes a line break
        /// </summary>
        /// <param name="text">Text to initiate the line break</param>
        /// <param name="pos">Position to break the line at</param>
        /// <param name="max">Maximum word to break line at</param>
        /// <returns>System.Int32</returns>
        private static int BreakLine(string text, int pos, int max)
        {
            int i = max;

            while (i >= 0 && !Char.IsWhiteSpace(text[pos + i])) {
                i--;
            }

            if (i < 0) {
                return max;
            }

            while (i >= 0 && Char.IsWhiteSpace(text[pos + i])) {
                i--;
            }

            return i + 1;
        }
    }
}
