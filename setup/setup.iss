#define AppName "AnubisBot"
#define Publisher "Kaleb Klein"
#define AppURL "https://kalebklein.com"
#define AppExeName "AnubisBot.exe"
#define VCLStylesSkinPath "{localappdata}\VCLStylesSkin"

#include "setup_code.pas"

[Setup]
AppId={{DC84546E-BE3A-4272-A941-CB6ED15C8491}
AppName={#AppName}
AppVersion={#Version}
AppVerName={#AppName} {#Version}
VersionInfoProductTextVersion={#Version}
VersionInfoCopyright=Copyright © 2017
AppPublisher={#Publisher}
AppPublisherURL={#AppURL}
AppSupportURL={#AppURL}
AppUpdatesURL={#AppURL}
DefaultDirName={userappdata}\{#AppName}
DisableDirPage=yes
DefaultGroupName={#AppName}
DisableProgramGroupPage=yes
OutputDir=Installer
OutputBaseFilename=anubisbot_setup-{#Version}
Compression=lzma
SolidCompression=yes
UninstallDisplayName={#AppName}_Uninstaller
UninstallDisplayIcon={app}\{#AppExeName}
WizardImageFile="C:\Program Files (x86)\The Road To Delphi\VCL Styles Inno\Images\WizModernImage-IS.bmp"
WizardSmallImageFile="C:\Program Files (x86)\The Road To Delphi\VCL Styles Inno\Images\WizModernSmallImage-IS.bmp"
DisableWelcomePage=no

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "..\bin\Release\net461\win10-x64\AnubisBot.exe"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\bin\Release\net461\win10-x64\AnubisBot.exe.config"; DestDir: "{app}"; Flags: ignoreversion
Source: "..\bin\Release\net461\win10-x64\*.dll"; DestDir: "{app}"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\af\Humanizer.resources.dll"; DestDir: "{app}\af"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\ar\Humanizer.resources.dll"; DestDir: "{app}\ar"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\bg\Humanizer.resources.dll"; DestDir: "{app}\bg"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\bn-BD\Humanizer.resources.dll"; DestDir: "{app}\bn-BD"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\cs\Humanizer.resources.dll"; DestDir: "{app}\cs"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\da\Humanizer.resources.dll"; DestDir: "{app}\da"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\de\Humanizer.resources.dll"; DestDir: "{app}\de"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\el\Humanizer.resources.dll"; DestDir: "{app}\el"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\es\Humanizer.resources.dll"; DestDir: "{app}\es"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\fa\Humanizer.resources.dll"; DestDir: "{app}\fa"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\fi-FI\Humanizer.resources.dll"; DestDir: "{app}\fi-FI"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\fr\Humanizer.resources.dll"; DestDir: "{app}\fr"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\fr-BE\Humanizer.resources.dll"; DestDir: "{app}\fr-BE"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\he\Humanizer.resources.dll"; DestDir: "{app}\he"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\hr\Humanizer.resources.dll"; DestDir: "{app}\hr"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\hu\Humanizer.resources.dll"; DestDir: "{app}\hu"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\id\Humanizer.resources.dll"; DestDir: "{app}\id"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\it\Humanizer.resources.dll"; DestDir: "{app}\it"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\ja\Humanizer.resources.dll"; DestDir: "{app}\ja"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\lv\Humanizer.resources.dll"; DestDir: "{app}\lv"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\nb\Humanizer.resources.dll"; DestDir: "{app}\nb"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\nb-NO\Humanizer.resources.dll"; DestDir: "{app}\nb-NO"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\nl\Humanizer.resources.dll"; DestDir: "{app}\nl"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\pl\Humanizer.resources.dll"; DestDir: "{app}\pl"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\pt\Humanizer.resources.dll"; DestDir: "{app}\pt"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\ro\Humanizer.resources.dll"; DestDir: "{app}\ro"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\ru\Humanizer.resources.dll"; DestDir: "{app}\ru"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\sk\Humanizer.resources.dll"; DestDir: "{app}\sk"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\sl\Humanizer.resources.dll"; DestDir: "{app}\sl"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\sr\Humanizer.resources.dll"; DestDir: "{app}\sr"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\sr-Latn\Humanizer.resources.dll"; DestDir: "{app}\sr-Latn"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\sv\Humanizer.resources.dll"; DestDir: "{app}\sv"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\tr\Humanizer.resources.dll"; DestDir: "{app}\tr"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\uk\Humanizer.resources.dll"; DestDir: "{app}\uk"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\uz-Cyrl-UZ\Humanizer.resources.dll"; DestDir: "{app}\uz-Cyrl-UZ"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\uz-Latn-UZ\Humanizer.resources.dll"; DestDir: "{app}\uz-Latn-UZ"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\vi\Humanizer.resources.dll"; DestDir: "{app}\vi"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\zh-CN\Humanizer.resources.dll"; DestDir: "{app}\zh-CN"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\zh-Hans\Humanizer.resources.dll"; DestDir: "{app}\zh-Hans"; Flags: ignoreversion
; Source: "..\bin\Release\net461\win10-x64\zh-Hant\Humanizer.resources.dll"; DestDir: "{app}\zh-Hant"; Flags: ignoreversion
Source: "redist\dotnet461_websetup.exe"; DestDir: "{app}\redist"; Flags: ignoreversion deleteafterinstall; Check: IsReqDotNetDetected

;========
; Styles
;========
Source: "C:\Program Files (x86)\The Road To Delphi\VCL Styles Inno\VclStylesinno.dll"; DestDir: {#VCLStylesSkinPath};
Source: "C:\Program Files (x86)\The Road To Delphi\VCL Styles Inno\Styles\Glow.vsf"; DestDir: {#VCLStylesSkinPath};

[Icons]
Name: "{group}\{#AppName}"; Filename: "{app}\{#AppExeName}"
Name: "{commondesktop}\{#AppName}"; Filename: "{app}\{#AppExeName}"; Tasks: desktopicon

[Run]
Filename: "{app}\redist\dotnet461_websetup.exe"; Parameters: "/passive /norestart"; Check: not IsReqDotNetDetected; StatusMsg: "Installing .NET 4.6.1, please wait..."

[UninstallDelete]
Type: files; Name: "{app}\config.json"
