[Code]
// Import the LoadVCLStyle function from VclStylesInno.DLL
procedure LoadVCLStyle(VClStyleFile: String); external 'LoadVCLStyleW@files:VclStylesInno.dll stdcall setuponly';
procedure LoadVCLStyle_UnInstall(VClStyleFile: String); external 'LoadVCLStyleW@{#VCLStylesSkinPath}\VclStylesInno.dll stdcall uninstallonly';
// Import the UnLoadVCLStyles function from VclStylesInno.DLL
procedure UnLoadVCLStyles; external 'UnLoadVCLStyles@files:VclStylesInno.dll stdcall setuponly';
procedure UnLoadVCLStyles_UnInstall; external 'UnLoadVCLStyles@{#VCLStylesSkinPath}\VclStylesInno.dll stdcall uninstallonly';

const
    NET_FW_SCOPE_ALL = 0;
    NET_FW_IP_VERSION_ANY = 2;

// Handle windows firewall exceptions
procedure SetFirewallException(AppName, FileName: string);
var
    FirewallObject: variant;
    FirewallManager: variant;
    FirewallProfile: variant;
begin
    try
        FirewallObject := CreateOleObject('HNetCfg.FwAuthorizedApplication');
        FirewallObject.ProcessImageFileName := FileName;
        FirewallObject.Name := AppName
        FirewallObject.Scope := NET_FW_SCOPE_ALL;
        FirewallObject.IpVersion := NET_FW_IP_VERSION_ANY;
        FirewallObject.Enabled := True;
        FirewallManager := CreateOleObject('HNetCfg.FwMgr');
        FireWallProfile := FirewallManager.LocalPolicy.CurrentProfile;
        FirewallProfile.AuthorizedApplications.Add(FirewallObject);
    except
    end;
end;

// Remove exception when uninstalled
procedure RemoveFirewallException(FileName: string);
var
    FirewallManager: variant;
    FirewallProfile: variant;
begin
    try
      FirewallManager := CreateOleObject('HNetCfg.FwMgr');
      FirewallProfile := FirewallManager.LocalPolicy.CurrentProfile;
      FirewallProfile.AuthorizedApplications.Remove(FileName);
    except
    end;
end;

procedure CurStepChanged(CurStep: TSetupStep);
begin
    if CurStep=ssPostInstall then begin
        SetFirewallException('AnubisBot', ExpandConstant('{app}\')+'AnubisBot.exe');
    end;
end;

procedure CurUninstallStepChanged(CurUninstallStep: TUninstallStep);
begin
    if CurUninstallStep=usPostUninstall then begin
        RemoveFirewallException(ExpandConstant('{app}\')+'AnubisBot.exe');
    end;
end;

function IsDotNetDetected(): boolean;
var
    key: string;
    release: cardinal;
begin
    key := 'SOFTWARE\Microsoft\NET Framework Setup\NDP\v4\Full'
    RegQueryDWordValue(HKLM, key, 'Release', release);

    // 394254 = 4.6.1 on Windows 10
    // other windows OS versions are higher than this

    if (release >= 394254) then
        result := true
    else
        result := false
end;

function IsReqDotNetDetected(): Boolean;
begin
    result := IsDotNetDetected();
end;

function InitializeSetup(): Boolean;
begin
    ExtractTemporaryFile('Glow.vsf');
    LoadVCLStyle(ExpandConstant('{tmp}\Glow.vsf'));

    if not IsDotNetDetected() then begin
        MsgBox('{#AppName} requires Microsoft .NET Framework 4.6.1.'#13#13
            'The installer will attempt to install it for you.', mbInformation, MB_OK);
    end;

    result := true;
end;

procedure DeinitializeSetup();
begin
    UnLoadVCLStyles;
end;

function InitializeUninstall: Boolean;
begin
    LoadVCLStyle_UnInstall(ExpandConstant('{#VCLStylesSkinPath}\Amakrits.vsf'));
    result := true;
end;

procedure DeinitializeUninstall();
begin
    UnLoadVCLStyles_UnInstall;
end;
