﻿using ServiceStack.DataAnnotations;
using System;

namespace AnubisBot.Sql
{
    [Alias("guilds")]
    public class Guilds
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public string name { get; set; }
        public ulong guild_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    [Alias("bot_spam")]
    public class BotSpam
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public ulong guild_id { get; set; }
        public ulong channel_id { get; set; }
        public bool is_enabled { get; set; }
        public string join_message { get; set; }
        public string leave_message { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    [Alias("restricted_roles")]
    public class RestrictedRoles
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
        public ulong guild_id { get; set; }
        public ulong role_id { get; set; }
        public DateTime created_at { get; set; }
        public DateTime updated_at { get; set; }
    }

    [Alias("lastfm")]
    public class Lastfm
    {
        [PrimaryKey, AutoIncrement]
        public int id { get; set; }
    }
}
