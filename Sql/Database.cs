﻿using AnubisBot.Utils;
using ServiceStack.OrmLite;
using System.Data;

namespace AnubisBot.Sql
{
    public class Database
    {
        /// <summary>
        /// Opens a database connection
        /// </summary>
        /// <returns>System.Data.IDbConnection</returns>
        public static IDbConnection Connect()
        {
            var dbc = Settings.getInstance().config.Database;
            var dsn = $"Server={dbc.Host};Port={dbc.Port};UID={dbc.Username};Password={dbc.Password};Database={dbc.DatabaseName};SslMode=none";
            var factory = new OrmLiteConnectionFactory(dsn, MySqlDialect.Provider);

            return factory.Open();
        }
    }
}
